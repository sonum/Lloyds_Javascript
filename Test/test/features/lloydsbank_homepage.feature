#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: LLoydsBank Javascript Automation


    Scenario Outline: Navigate to Lloyds bank homepage and navigate to login page for both personal and business bank    
				Given user is on Lloyds Bank <URL> Homepage
				When user navigate to personal login page
				Then verify user is navigated to personal Log on page
				When user is again on Lloyds Bank <URL> Homepage
				#Then user navigate to business login page
				#Then verify user is navigated to personal Log on page
				 
  			Examples:
				|URL|type|
				|"https://www.lloydsbank.com"|"Personal"|
#	@tag2			
#    Scenario Outline: Performing a search operation
#   		Given user is on google home page
#      	When user enter <search Item> into the search box
#     	And  user click the search button
#   	 	Then user should see a list of search results
# 			Then user should navigate to homepage of <search Item>
    
       # Examples:
       # |search Item|
       # |"Halifax"|
