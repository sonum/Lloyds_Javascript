
import Page from './page';

class lloydsbank_homepage extends Page  {
  /**
  * define elements
  */

  //get personalLoginButton()   { return browser.element('//*[@id="c1425473242096"]/div/div/div[3]/div/div/div/div[2]/ul/li[4]/a'); }
  //get personal()  { return browser.element('//div[@class="mag-glass"]'); }
  get businessLoginLink()   { return browser.element('//a[@class="button button-primary"]'); }
  get loginpage() {return browser.element('//*[@id="frmLogin:strCustomerLogin_userID"]');}
  get personalLoginLink() {return browser.element('//*[@id="c1425473242018"]/div/ul[1]/li[1]/a');}
  /**
   * define or overwrite page methods
   */

  open (URL) {
	  
      super.open(URL);       //provide your additional URL if any. this will append to the baseUrl to form complete URL
      browser.pause(1000);
  }

  moveToObj () {
    this.loginButton.moveToObject();
    //browser.moveToObject(loginbutton);
  }
  waitForloginPageToLoad () {
      if(!this.personalLoginLink.isVisible()){
        this.personalLoginLink.waitForVisible(5000);
      }
    }
  
  
  clickPersonalLink() {
	//this.waitForloginPageToLoad();
    this.personalLoginLink.click();
   // puts("clicked on login link3")}
  }
  
  clickBusinessLink() {
	  //this.waitForloginPageToLoad();
    this.LoginLink.click();}
	 
  validate () {
    this.loginpage.waitForVisible(5000);
    return this.loginpage.isVisible();
  }
}

export default new lloydsbank_homepage();
